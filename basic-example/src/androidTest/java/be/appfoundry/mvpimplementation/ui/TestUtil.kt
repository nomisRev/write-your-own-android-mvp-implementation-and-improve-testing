package be.appfoundry.mvpimplementation.ui

import android.support.design.widget.Snackbar
import android.widget.TextView
import be.appfoundry.mvpimplementation.R

fun Snackbar.getText() : CharSequence {

  val count = (this.view as Snackbar.SnackbarLayout).childCount
  for (i in 0..(count -1)) {
    if ((this.view as Snackbar.SnackbarLayout).getChildAt(i).id == R.id.snackbar_text){
      return ((this.view as Snackbar.SnackbarLayout).getChildAt(i) as TextView).text
    }
  }
  return "null"
}